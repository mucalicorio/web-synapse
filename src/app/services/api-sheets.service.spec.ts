import { TestBed } from '@angular/core/testing';

import { ApiSheetsService } from './api-sheets.service';

describe('ApiSheetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiSheetsService = TestBed.get(ApiSheetsService);
    expect(service).toBeTruthy();
  });
});
