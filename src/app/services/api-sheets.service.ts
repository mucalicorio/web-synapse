import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiSheetsService {

  SPREEDSHEET_ID = '1_6Z7F0WUzEQS3QnT9rtUlHEziWe5DAHRrFpso-uEloY';

  GET_URL = `https://sheets.googleapis.com/v4/spreadsheets/${this.SPREEDSHEET_ID}/values/`;

  API_KEY = 'AIzaSyDm5Z7Co_5Th3y-WWCUf_s6xY-QNzd7dg8';

  constructor(private httpClient: HttpClient) { }

  public getApiSpreadsheets(range): Observable<any> {
    return this.httpClient.get(`${this.GET_URL}${range}`, { params: { key: this.API_KEY } });
  }
}
