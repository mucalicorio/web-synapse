import { Component, Output, EventEmitter } from '@angular/core';

import { ApiSheetsService } from './services/api-sheets.service';

import { EChartOption } from 'echarts';


export class TiposGraficos {
  name: string;
}

interface RenderedChart {
  type: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'webSynapse';

  headerApi;
  values;

  toggleMenu = true;

  chartOption: EChartOption;

  public chartsTypes: Array<TiposGraficos>;

  // public charts: Array<>;

  // @Output
  // click: EventEmitter<>

  constructor(private apiSheetsService: ApiSheetsService) {
    this.getHeaderApi();

    this.chartsTypes = [
      { name: 'Linhas' },
      { name: 'Barras' },
      { name: 'Pizza' },
      { name: 'Bolinha' }
    ];
  }


  getHeaderApi() {
    this.values = this.apiSheetsService.getApiSpreadsheets('A1:I1')
      .subscribe(data => {
        this.headerApi = data.values;
        console.log('Header Api: ', this.headerApi);
      });
  }


  getValuesApi() {
    this.values = this.apiSheetsService.getApiSpreadsheets('A2:I')
      .subscribe(data => {
        this.values = data.values;
        console.log('Values: ', this.values);

        for (let value = 0; value < this.values[0].length; value++) {
          console.log('Value: ', value);
        }

        this.chartOption = {
          xAxis: {
            type: 'category',
            data: ['ooo', 'iii', '8yj']
          },
          yAxis: {
            type: 'value'
          },
          series: [{
            data: [22, 5, 92],
            type: 'line'
          }]
        };

      }, err => {
        console.log('Error: ', err);
      });
  }


  // selectItem(item) {
  //   this.click.item(item);
  // }
}
